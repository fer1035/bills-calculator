#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""Calculate monthly budget."""
import json


def lambda_handler(event, context):
    """
    Calculate monthly budget

    return dict
    """
    income = float(json.loads(event['body'])['income'])
    additional = float(json.loads(event['body'])['additional'])

    bills = {
        'centrus': 5000.00,
        'additional': additional,
        'car': 5000.00,
        'monthly1': 1000.00,
        'monthly2': 1000.00,
        'bustana': 1000.00
    }
    credit = {
        'celcom': 400.00,
        'tnb': 600.00,
        'astro': 100.00,
        'indah_water': 8.00,
        'city': 300.00
    }
    advanced = {
        'netflix - auto': 50.00,
        'prudential - advanced': 1000.00,
        'centrus_service - advanced': 300.00,
        'water - advanced': 50.00
    }

    credit.update(advanced)
    bills_sum = sum(bills.values())
    balance = income - bills_sum
    credit_sum = sum(credit.values())

    data = {
        'bills': bills,
        'credit': credit,
        'overhead': {
            'cash': round(bills_sum - additional, 2),
            'credit': round(credit_sum, 2),
            'spent': round(additional - credit_sum, 2)
        },
        'summary': {
            'overhead': round(bills_sum, 2),
            'savings': round(balance, 2)
        }
    }

    # Return response.
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps(data, default=str)
    }
